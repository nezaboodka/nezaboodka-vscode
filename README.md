# Installation

1. Run:

```
# Windows: cd %HOMEPATH%/.vscode/extensions
cd ~/.vscode/extensions
git clone https://gitlab.com/nezaboodka/nezaboodka-vscode.git
```

2. Restart VS Code.

3. Press `Ctrl + K + T` and select `Nezaboodka` color theme.
